import firebase from 'firebase/app';
import 'firebase/database';

const firebaseConfig = {
    apiKey: "AIzaSyBg8gMcNxZ-T_rS2oDA0rA7cSjsIEbaqFw",
    authDomain: "devpleno-comentarios-108d8.firebaseapp.com",
    databaseURL: "https://devpleno-comentarios-108d8.firebaseio.com",
    projectId: "devpleno-comentarios-108d8",
    storageBucket: "",
    messagingSenderId: "301022140555",
    appId: "1:301022140555:web:f1973b9c584e38eb"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const database = firebase.database();