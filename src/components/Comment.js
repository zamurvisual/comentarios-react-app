import React from 'react'

// se tornou uma pure function, ou seja, ela só retorna sobre o que eu injetar de valor
// o retorno será puro e exclusivamente baseado no comment, que é o único parametro desta pure function
const Comment = ({ comment }) => {
    return (
        <div>
            <div>Comentário: { comment.comment }</div>
        </div>
    )
}

export default Comment





// import React, { Component } from 'react'

// class Comment extends Component {
//     render() {
//         return (
//             <div>
//                 <div>Comentário: {  this.props.comment.comment }</div>
//             </div>
//         )
//     }
// }

// export default Comment
