import React from 'react'
import Comment from './Comment'


// destructuring assignment nos paramentro
// viriam as props, mas desconstruindo ela, já consigo pegar diretamete a variavel comments que viria dentro dela
// comments é a unica coisa de props que será utilizada
const Comments = ({ comments }) => {
    const keys = Object.keys(comments);
    return (
        <div>
            { keys.map(key => <Comment key={key} comment={comments[key]} /> )}
        </div>
    )
}

export default Comments



// import React, { Component } from 'react';
// import Comment from './Comment';

//  class Comments extends Component {
//     render() {
//         const keys = Object.keys(this.props.comments);

//         return (
//             <div>
//                 { keys.map(key => <Comment key={key} comment={this.props.comments[key]} /> )}
//             </div>
//         )
//     }
// }

// export default Comments;